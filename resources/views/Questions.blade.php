@extends('master')
@section('content')

    <head>
        <link rel="stylesheet" href="{{asset('button')}}">
        <style>
            .img
            {
                background-repeat: no-repeat;
            }

             .correct-selected{
                 background-color: #d4edda;
             }
        </style>
    </head>
 <body background="/images/back_7.jpg">
    <center><h1 style="font-size: 60px; color: black;">Додати анкету</h1></center>
    <form method="post" name="add_name" id="add_name">
        @csrf
    <div class="container">
        <div class="form-row">
            <div class="col">
                <select class="dropdown-toggle form-control" name="user_id">
                    <option disabled>Виберіть кандидата</option>
                    @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col">
                <input type="text" class="w3-input form-control" placeholder="Посада!" id="position" name="position" required>
            </div>

        </div>
    </div>
    <br />

    <div class="container text-center">
            <div class="form-group" id="dynamic_field">
                 <input class="w3-input form-control" type="text" placeholder="Введіть питання!" id="test" name="question_1" required>
                <br />
                <span id="err" style="font-size: 20px; color: #5cd08d; font-weight: bold;"></span>
                <span id="error" style="font-size: 20px; color: red; font-weight: bold;"></span>

            </div>
                {{-- add/remove form --}}
                <script>
                    $(document).ready(function () {
                        var i = 1;
                        $('#add').click(function () {
                            i++;
                            $('#dynamic_field').append('<div id="row'+i+'">' +
                                '<br /><input class="w3-input form-control" type="text" placeholder="Введіть питання!" id="question" name="question_'+i+'" required>' +
                                '</div>');
                        });
                        $(document).on('click','.btn_remove', function () {
                            var button_id = $(this).attr("id");
                            $('#row'+button_id+'').remove();
                        })
                    });

                    $(document).ready(function () {
                        var i = 1;
                        $('#add').click(function () {
                            i++;
                            $('#dynamic_field').append('<div id="row'+i+'"><br />' +
                                '<div class="form-check">' +
                                '<input type="checkbox" class="custom-control-input" id="checkbox_1" name="checkbox1">' +
                                '<span id="err" style="font-size: 20px; color: #5cd08d; font-weight: bold;"></span>' +
                                '<span id="error" style="font-size: 20px; color: red; font-weight: bold;"></span>' +
                                '<button class="btn btn-danger btn_remove" name="remove" id="'+i+'">Видалити форму</button></div>');
                        });
                        $(document).on('click','.btn_remove', function () {
                            var button_id = $(this).attr("id");
                            $('#row'+button_id+'').remove();
                        })
                    });
                </script>
        <button type="button" class="button" id="result" name="results[]" onclick="answers()">Результат</button>
        <button class="button" name="add" id="add">Додати форму</button>
        <button class="button destroy">Видалити дані з форми</button>
            <button class="button btn-warning" name="add" id="add">Зберегти анкету</button>
    </div>
    </form>


    {{-- save/destroy data in form --}}
{{--    <script src="/js/jquery-3.2.1.min.js"></script>--}}
{{--    <script src="/js/savy.min.js"></script>--}}
{{--    <script>--}}
{{--        $(function(){--}}
{{--            $('.w3-input').savy('load');--}}
{{--            $('.destroy').click(function(){--}}
{{--                $('.w3-input').savy('destroy');--}}
{{--            })--}}
{{--        })--}}
{{--    </script>--}}
    <br>
 </body>
@endsection

