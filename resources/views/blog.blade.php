<!DOCTYPE html>
<html lang="en-US" class="no-js no-svg">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Blog</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('/css/blog.css')}}">

    <link rel='stylesheet' id='twentyseventeen-fonts-css'
          href='https://fonts.googleapis.com/css?family=Libre+Franklin%3A300%2C300i%2C400%2C400i%2C600%2C600i%2C800%2C800i&#038;subset=latin%2Clatin-ext'
          type='text/css' media='all' />
    <link rel='stylesheet' id='twentyseventeen-style-css'
          href='https://blog.laravelecommerceexample.ca/wp-content/themes/twentyseventeen/style.css?ver=4.9.13'
          type='text/css' media='all' />
    <script type="text/javascript" src="/js/search.js"></script>
    <script type='text/javascript' src='https://blog.laravelecommerceexample.ca/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>

    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
</head>

<body class="home blog hfeed has-header-image has-sidebar colors-light">

<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
    <header id="masthead" class="site-header" role="banner">
        <div class="custom-header">
            <div class="custom-header-media">
                <div id="wp-custom-header" class="wp-custom-header">
                    <img src="/images/blog.jpg" width="2000" height="1200" alt="Blog" />
                </div>
            </div>
            <div class="site-branding">
                <div class="wrap">
                    <div class="site-branding-text">
                        <h1 class="site-title">
                            <a href="{{asset('blog')}}" rel="home">
                                Блоги для онлайн співбесід
                            </a>
                        </h1>
                    </div>
                    <br /><br />
                    <center>
                        <a href="#hillel">
                            <i class="fa fa-angle-double-down" style="font-size: 72px;color:white"></i>
                        </a>
                    </center>
                </div>
            </div>
        </div>
    </header>
    <div class="site-content-contain">
        <div id="content" class="site-content">
            <div class="wrap">
                <div id="primary" class="content-area">
                            <header class="entry-header">
                                <h3 class="entry-title">
                                    <a href="#" rel="bookmark" id="hillel">
                                        HILLEL
                                    </a>
                                </h3>
                            </header>

                            <div class="post-thumbnail">
                                <a href="https://blog.ithillel.ua/ua/articles/yak-proity-spivbesidu-v-it-kompanii">
                                    <img width="1920" height="1080" alt="Blog"
                                         src="/images/hilel_blog.jpg" />
                                </a>
                            </div>

                            <div class="entry-content">
                                <p>Закінчивши курси програмування або тестування, ви повні рішучості починати роботу в IT-компанії.
                                    Але пройшли першу, другу, третю співбесіду,
                                    а вам усе відмовляють… і ви у чималому подиві: “Що я роблю не так?”.
                                    Зазвичай, HR-спеціалісти повідомляють причину відмови,
                                    але істинну проблему можуть не сказати. Ми хочемо поділитися з вами кількома порадами,
                                    про які варто пам’ятати при підготовці до співбесіди.
                                    І так, до співбесіди необхідно готуватися :)</p>
                                <p>Почнемо із загальних, але дуже важливих порад з підготовки до співбесіди:
                                  <ul>
                                    <li>Ознайомтесь з історією та специфікою роботи IT-компанії, в якій ви хочете працювати.</li>
                                    <li>Визначтесь з конкретною посадою, на яку ви претендуєте.</li>
                                    <li>Будьте готові розказати про свій досвід.</li>
                                  </ul>
                                </p>
                            </div>

                            <header class="entry-header">
                                <h3 class="entry-title">
                                    <a href="#" rel="bookmark">
                                        the UKRAINIANS
                                    </a>
                                </h3>
                            </header>

                            <div class="post-thumbnail">
                                <a href="https://theukrainians.org/10-pomylok-spivbesidy/">
                                    <img width="1920" height="1000" alt="Blog"
                                         src="/images/Ukrainians_blog.jpg" />
                                </a>
                            </div>

                            <div class="entry-content">
                                <p>Проходження співбесіди є важливим компонентом успішної кар’єри і мистецтвом, яким володіє не кожен.
                                    Більшість людей нервують, коли віч-на-віч зустрічаються з групою експертів, які вирішуватимуть,
                                    брати вас на роботу чи ні.</p>
                                <p>Більшість людей намагаються грати ролі, присвоюючи собі манери, що їм не притаманні.
                                    Результати, відповідно, — дуже погані.</p>
                            </div>

                            <header class="entry-header">
                                <h3 class="entry-title">
                                    <a href="#" rel="bookmark">
                                        DAN.IT
                                    </a>
                                </h3>
                            </header>

                            <div class="post-thumbnail">
                                <a href="https://dan-it.com.ua/uk/populjarni-zapitannja-pri-prohodzhenni-spivbesidi/">
                                    <img width="1920" height="1080" alt="Blog"
                                         src="/images/dan.it_blog.jpg" />
                                </a>
                            </div>

                            <div class="entry-content">
                                <p>Що таке співбесіда?</p>
                                <p>Це зовсім не страшне опитування, де ви загнані в куточок і повинні відповідати,
                                    як під дулом пістолета, це навіть не оцінка, і не допит.
                                    Саме слово співбесіда натякає на приємну бесіду з співрозмовником,
                                    звичайно не про погоду за вікном, та все ж задоволення повинні отримати обидві сторони.
                                    Проблема лише в тім, що неможливо за короткий проміжок спілкування оцінити всі навички і професійність людини,
                                    тому в цьому випадку допомагають ситуативні питання, власне враження та славнозвісні soft skills.</p>
                            </div>

                            <header class="entry-header">
                                <h3 class="entry-title">
                                    <a href="#" rel="bookmark">
                                        GUID
                                    </a>
                                </h3>
                            </header>

                            <div class="post-thumbnail">
                                <a href="https://guid.com.ua/ua/blog/it_interviews/">
                                    <img width="1920" height="1080" alt="Blog"
                                         src="/images/guid_blog.png" />
                                </a>
                            </div>

                            <div class="entry-content">
                                <p>Попередні статті за підсумками Candidate Experience Research продемонстрували:
                                    процес найму в Україні місцями є ірраціональним і не завжди враховує інтереси кандидатів.</p>
                                <p>Ми розпитали IT-фахівців, як часто на інтерв’ю в IT-компаніях вони стикаються з:
                                <ul>
                                    <li>співбесідою «в режимі допиту»</li>
                                    <li>психологічними методиками відбору.</li>
                                    <li>«кодінгом на листочку»</li>
                                    <li>слабким технічним рівнем інтерв’юера.</li>
                                    <li>поганою підготовленістю інтерв’юера.</li>
                                    <li>спробами принизити свій професійний рівень.</li>
                                </ul>
                                </p>
                            </div>

                            <header class="entry-header">
                                <h3 class="entry-title">
                                    <a href="#" rel="bookmark">
                                        LOGOS
                                    </a>
                                </h3>
                            </header>

                            <div class="post-thumbnail">
                                <a href="https://lgs.lviv.ua/pidgotovka-spivbesidy-pid-yakym-sousom-sebe-podavaty/">
                                    <img width="1920" height="1080" alt="Blog"
                                         src="/images/logos_blog.jpg" />
                                </a>
                            </div>

                            <div class="entry-content">
                                <p>Пошук чи зміна робочого місця – це завжди стрес.
                                    Звісно ж, наш шлях до нової кар’єри розпочинається із написання CV або ж внесення змін до старого резюме,
                                    якщо у Вас вже був попередній досвід роботи.
                                    І от коли ми вже відібрали вакансії,
                                    які нас зацікавили та розіслали пропозицію власної кандидатури роботодавцям нам приходить запрошення на співбесіду.
                                    Власне, від результатів співбесіди буде залежати результат працевлаштування.</p>
                                <p>В невеликих компаніях співбесіду, зазвичай, проводить керівник або його заступник.
                                    Якщо Ви нічого не перебільшили у резюме, та усвідомлюєте, які вимоги до Вас виставляють, тоді немає чого лякатись.
                                    На співбесіді не потрібно боятись потенційного роботодавця, варто задавати усі питання,
                                    які Вас цікавлять та пам’ятати, що в даний момент Ви продаєте свій час.</p>
                            </div>
                </div><!-- #primary -->

                <aside id="secondary" class="widget-area" role="complementary" aria-label="Blog Sidebar">
                    <div role="search" class="search-form">
                    <input type="text" id="text-to-find" placeholder="Search &hellip;"  />
                    <button type="submit" class="search-submit" onclick="javascript: FindOnPage('text-to-find'); return false;" value="Шукати">
                        <i class="fa fa-search"></i>
                    </button>
                    </div>
                        <br>
                    <section id="recent-posts-2" class="widget widget_recent_entries">
                        <h2 class="widget-title">IT-блоги</h2>
                        <ul>
                            <li>
                                <a href="https://blog.ithillel.ua/ua/articles/yak-proity-spivbesidu-v-it-kompanii">Hillel</a>
                            </li>
                            <li>
                                <a href="https://dan-it.com.ua/uk/populjarni-zapitannja-pri-prohodzhenni-spivbesidi/">DAN.IT</a>
                            </li>
                            <li>
                                <a href="https://guid.com.ua/ua/blog/it_interviews/">Guid</a>
                            </li>
                            <li>
                                <a href="https://lgs.lviv.ua/pidgotovka-spivbesidy-pid-yakym-sousom-sebe-podavaty/">Logos</a>
                            </li>
                            <li>
                                <a href="https://hurma.work/blog/yak-provesti-spivbesidu-onlajn-ta-5-program-de-cze-mozhna-zrobiti/">Hurma</a>
                            </li>
                            <li>
                                <a href="https://www2.deloitte.com/ua/uk/pages/press-room/blog-of-chat-bot-dtalca/behavioral-interview.html">Deloitte.</a>
                            </li>
                            <li>
                                <a href="https://www.launchcode.org/blog/top-tips-for-your-tech-interview/">launch_code</a>
                            </li>
                        </ul>
                    </section>
                    <section id="recent-comments-2" class="widget widget_recent_comments">
                        <h2 class="widget-title">Інтерв'ю в блозі</h2>
                        <ul id="recentcomments">
                            <li class="recentcomments">
                                <span class="comment-author-link">
                                    <a href='https://www.codeinwp.com/blog/guide-to-blog-interviews/' rel='external nofollow' class='url'>
                                        Codeinwp
                                    </a>
                                </span>
                            </li>
                            <li>
                                <a href="https://www.incomediary.com/10-tips-creating-awesome-interviews-blog">
                                    Income
                                </a>
                            </li>
                            <li>
                                <a href="https://www.impactbnd.com/blog/why-interview-based-blogging">
                                    Impact
                                </a>
                            </li>
                            <li>
                                <a href="https://fizzle.co/sparkline/blog-interviews">
                                    Fizzle
                                </a>
                            </li>
                            <li>
                                <a href="http://onlineincometeacher.com/content/advantages-of-writing-online-blog-interviews/">
                                    Online Income Teacher
                                </a>
                            </li>
                        </ul>
                    </section>
                    <section id="meta-2" class="widget widget_meta">
                        <h2 class="widget-title">Блоги для менеджерів</h2>
                        <ul>
                            <li><a href="http://www.management.com.ua/blog/1313">Management</a></li>
                            <li><a href="http://aphd.ua/spivbesida---tse-zavzhdy-tsikavo/">APhD</a></li>
                            <li>
                                <a href="https://nv.ua/ukr/style/blogs/12-najpopuljarnishih-pomilok-pochatkivtsiv-biznesmeniv-blog-startap-menedzhera-1595362.html">
                                    HB Style
                                </a>
                            </li>
                            <li>
                                <a href="https://www.arthuss.com.ua/books-blog/5-knyzhok-dlya-menedzheriv-mystetstva-vid-kateryny-pidhaynoyi">
                                    ArtHuss
                                </a>
                            </li>
                            <li>
                                <a href="https://senior.ua/articles/40-cnnih-resursv-pro-prodaktmenedzhment">
                                    Senior.ua
                                </a>
                            </li>
                        </ul>
                    </section>

                    <section id="meta-2" class="widget widget_meta">
                        <h2 class="widget-title">Блоги</h2>
                        <ul>
                            <li>
                                <a href="https://smartblogger.com/how-to-interview/">
                                    SmartBlogger
                                </a>
                            </li>
                            <li>
                                <a href="https://www.coburgbanks.co.uk/blog/assessing-applicants/11-interview-questions-that-will-reveal-your-candidates-true-personality/">
                                    CoburgBanks
                                </a>
                            </li>
                            <li>
                                <a href="https://novoresume.com/career-blog/interview-questions-and-best-answers-guide">
                                    Novoresume
                                </a>
                            </li>
                            <li>
                                <a href="https://biginterview.com/blog/">
                                    Big Interview
                                </a>
                            </li>
                            <li>
                                <a href="https://www.breakingtheonepercent.com/blog-interview-series/">
                                    Breaking
                                </a>
                            </li>
                        </ul>
                    </section>
                </aside><!-- #secondary -->
            </div><!-- .wrap -->
        </div><!-- #content -->

        <center>
            <a href="{{asset('index')}}">
                <button class="button">Головна</button>
            </a>
        </center>

        <footer id="colophon" class="site-footer" role="contentinfo">
            <div class="wrap">

                <div class="site-info">
                    <a href="#" class="imprint">
                        © <?php $date = date('Y'); echo $date;?> Interview</a>
                </div>
            </div>
        </footer>
    </div>
</div>
<script type='text/javascript' src='https://blog.laravelecommerceexample.ca/wp-content/themes/twentyseventeen/assets/js/global.js?ver=1.0'></script>
</body>
</html>
