@extends('master')
@section('content')

    <head>
        <link rel="stylesheet" href="{{asset('button')}}">
        <style>
            .img
            {
                background-repeat: no-repeat;
            }

             .correct-selected{
                 background-color: #d4edda;
             }
        </style>
    </head>
 <body background="/images/back_7.jpg">
    <center><h1 style="font-size: 60px; color: black;">Відповісти на анкету</h1></center>
    <form method="post" name="add_name" id="add_name">
        @csrf
    <div class="container">
        <div class="form-row">
            <div class="col">
                <input type="date" class="w3-input form-control" id="date" name="date[]">
            </div>
            <div class="w3-input col">
                <input id="my_timer" value="00:00:00" class="form-control form-control-sm text-center" type="text" style="font-size: 15px;" size="8" name="StartTime[]">
            </div>
        </div>
    </div>
    <br />

    <div class="container text-center">
            <div class="form-group" id="dynamic_field">
                @foreach($questions as $question)
                <input class="w3-input form-control" type="text" placeholder="Введіть питання!" id="test" name="question_{{$question->question_id}}" value="{{$question->question}}" disabled>
                <br />
                <div class="form-check">

                    @if (!$question->answer_id)
                <textarea class="w3-input form-control" rows="5" placeholder="Введіть відповідь!" id="answer" name="answer_{{$question->question_id}}" required></textarea>
                    @else
                        @if($question->is_right === 1)
                            <span id="err" style="font-size: 20px; color: #5cd08d; font-weight: bold;">Відповідь вірна</span>
                        @elseif ($question->is_right === 0)
                            <span id="error" style="font-size: 20px; color: red; font-weight: bold;">Відповідь невірна!</span>
                        @endif
                            <input type="checkbox" class="custom-control-input" id="checkbox_{{$question->question_id}}" name="checkbox_{{$question->question_id}}" {{ $question->is_right === 1 ? 'checked' : ''}}>

                            <label class="custom-control-label" for="checkbox_{{$question->question_id}}">
                            <textarea class="w3-input form-control" rows="5" placeholder="Введіть відповідь!" id="answer" name="answer_{{$question->question_id}}_exist" disabled> {{$question->answer}}</textarea>
                            <input type="hidden" name="answer_{{$question->question_id}}_exist_id" value="{{$question->answer_id}}">
                            </label>

                    @endif
                </div>
                <span id="err" style="font-size: 20px; color: #5cd08d; font-weight: bold;"></span>
                <span id="error" style="font-size: 20px; color: red; font-weight: bold;"></span>
                <br />
                @endforeach
                <br />
                <button type="button" class="button" id="result" name="results[]" onclick="answers()">Результат</button>
                <button class="button" name="add" id="add">Додати форму</button>
                <button class="button destroy">Видалити дані з форми</button>
                <button class="button" id="control" onclick="changeState();">Час</button>
                <button class="button" id="reset" onclick="reset();">Видалити час</button>
                <script type="text/javascript" src="/js/timer.js"></script>
                <br /><br /><br />
            </div>
                {{-- add/remove form --}}
{{--                <script>--}}
{{--                    $(document).ready(function () {--}}
{{--                        var i = 1;--}}
{{--                        $('#add').click(function () {--}}
{{--                            i++;--}}
{{--                            $('#dynamic_field').append('<div id="row'+i+'">' +--}}
{{--                                '<br /><input class="w3-input form-control" type="text" placeholder="Введіть питання!" id="question" name="question_'+i+'" required>' +--}}
{{--                                '</div>');--}}
{{--                        });--}}
{{--                        $(document).on('click','.btn_remove', function () {--}}
{{--                            var button_id = $(this).attr("id");--}}
{{--                            $('#row'+button_id+'').remove();--}}
{{--                        })--}}
{{--                    });--}}

{{--                    $(document).ready(function () {--}}
{{--                        var i = 1;--}}
{{--                        $('#add').click(function () {--}}
{{--                            i++;--}}
{{--                            $('#dynamic_field').append('<div id="row'+i+'"><br />' +--}}
{{--                                '<div class="form-check">' +--}}
{{--                                '<input type="checkbox" class="custom-control-input" id="checkbox_1" name="checkbox1">' +--}}
{{--                                '<label class="custom-control-label" for="checkbox_1">' +--}}
{{--                                '<textarea class="w3-input form-control" rows="5" placeholder="Введіть відповідь!" id="answer" name="answer_'+i+'" required></textarea>' +--}}
{{--                                '</label>' +--}}
{{--                                '</div><br />' +--}}
{{--                                '<span id="err" style="font-size: 20px; color: #5cd08d; font-weight: bold;"></span>' +--}}
{{--                                '<span id="error" style="font-size: 20px; color: red; font-weight: bold;"></span>' +--}}
{{--                                '<button class="btn btn-danger btn_remove" name="remove" id="'+i+'">Видалити форму</button></div>');--}}
{{--                        });--}}
{{--                        $(document).on('click','.btn_remove', function () {--}}
{{--                            var button_id = $(this).attr("id");--}}
{{--                            $('#row'+button_id+'').remove();--}}
{{--                        })--}}
{{--                    });--}}
{{--                </script>--}}

            <button class="button btn-warning" name="add" id="add">Зберегти відповіді</button>
    </div>
    </form>

    <script>
        function answers()
        {
            var b=document.getElementById("checkbox");
            if(( b.checked == true))
            {
                document.getElementById('err').innerHTML= 'Відповідь вірна';
            }
            else
            {
                document.getElementById('error').innerHTML= 'Відповідь не вірна';
            }
        }
    </script>
    {{-- save/destroy data in form --}}
{{--    <script src="/js/jquery-3.2.1.min.js"></script>--}}
{{--    <script src="/js/savy.min.js"></script>--}}
{{--    <script>--}}
{{--        $(function(){--}}
{{--            $('.w3-input').savy('load');--}}
{{--            $('.destroy').click(function(){--}}
{{--                $('.w3-input').savy('destroy');--}}
{{--            })--}}
{{--        })--}}
{{--    </script>--}}
    <br>
 </body>
@endsection

