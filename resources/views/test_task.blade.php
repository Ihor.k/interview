@extends('master')
@section('content')

 <body background="/images/back_8.png">
   <center><h1 style="font-size: 60px; color: black;">Тестові завдання</h1></center>
   <div class="container">
     <div class="form-row">
         <div class="col">
             <input type="text" class="w3-save form-control" placeholder="Ім'я!" id="name" name="name[]" required>
         </div>
         <div class="col">
             <input type="text" class="w3-save form-control" placeholder="Прізвище!" id="surname" name="SurName[]" required>
         </div>
         <div class="col">
             <input type="text" class="w3-save form-control" placeholder="Посада!" id="position" name="position[]" required>
         </div>
         <div class="col">
             <input type="date" class="w3-save form-control" id="date" name="date[]">
         </div>
         <div class="w3-save col">
             <input id="my_timer" value="00:00:00" class="form-control form-control-sm text-center" type="text" style="font-size: 15px;" placeholder="Start time" size="8" name="StartTime">
         </div>
     </div>
   </div>
    <br />

  <form name="add_name" id="add_name">
    <div class="container">
      <div class="form-group" id="dynamic_field">
        <div class="text-center">
            <div class="form-check">
                <input type="checkbox" class="custom-control-input" id="checkbox" name="checkbox">
                <label class="custom-control-label" for="checkbox">
                <input type="text" class="w3-save form-control" placeholder="Введіть запитання!" name="question" required><br />
                </label>
            </div>
            <div class="form-check">
                    <input type="checkbox" class="w3-save custom-control-input" id="checkbox_2" name="checkbox2">
                    <label class="custom-control-label" for="checkbox_2">
                        <input type="text" class="w3-save form-control" placeholder="Введіть відповідь!" id="one" name="questionOne" required>
                    </label>
            </div><br />
            <div class="form-check">
                <input type="checkbox" class="w3-save custom-control-input" id="checkbox_3" name="checkbox3">
                <label class="custom-control-label" for="checkbox_3">
                    <input type="text" class="w3-save form-control" placeholder="Введіть відповідь!" id="two" name="questionTwo" required>
                </label>
            </div><br />
            <div class="form-check">
                <input type="checkbox" class="w3-save custom-control-input" id="checkbox_4" name="checkbox4">
                <label class="custom-control-label" for="checkbox_4">
                    <input type="text" class="w3-save form-control" placeholder="Введіть відповідь!" id="three" name="questionThree" required>
                </label>
            </div>
            <span id="err" style="font-size: 20px; color: #5cd08d; font-weight: bold;"></span>
            <span id="error" style="font-size: 20px; color: red; font-weight: bold;"></span>
            <br /><br />
            <button type="button" class="button" id="result" name="results[]" onclick="answers()">Результат</button>
            <button class="button" name="add" id="add">Додати форму</button>
            <button class="button destroy">Видалити дані з форми</button>
            <button class="button" id="control" onclick="changeState();">Start</button>
            <script type="text/javascript" src="/js/timer.js"></script>
            <br /><br /><br />
        </div>
      </div>
    </div>
  </form>

{{-- add/remove form test task --}}
    <script>
        $(document).ready(function () {
            var i = 1;
            $('#add').click(function () {
                i++;
                $('#dynamic_field').append('<div id="row'+i+'">' +
                    '<div class="form-check">\n' +
                    '<input type="checkbox" class="custom-control-input" id="checkbox_5" name="checkbox5">' +
                    '<label class="custom-control-label" for="checkbox_5">' +
                    '<input type="text" class="w3-save form-control" placeholder="Введіть запитання!" name="question" required><br />' +
                    '</label>' +
                    '</div>' +
                    '<div class="form-check">' +
                    '<input type="checkbox" class="w3-save custom-control-input" id="checkbox_6" name="checkbox6">' +
                    '<label class="custom-control-label" for="checkbox_6">' +
                    '<input type="text" class="w3-save form-control" placeholder="Введіть відповідь!" required>' +
                    '</label>' +
                    '</div><br />' +
                    '<div class="form-check">' +
                    '<input type="checkbox" class="w3-save custom-control-input" id="checkbox_7" name="checkbox7">' +
                    '<label class="custom-control-label" for="checkbox_7">' +
                    '<input type="text" class="w3-save form-control" placeholder="Введіть відповідь!" required>' +
                    '</label>' +
                    '</div><br />' +
                    '<div class="form-check">' +
                    '<input type="checkbox" class="w3-save custom-control-input" id="checkbox_8" name="checkbox8">' +
                    '<label class="custom-control-label" for="checkbox_8">' +
                    '<input type="text" class="w3-save form-control" placeholder="Введіть відповідь!" required>' +
                    '</label>' +
                    '<span id="err" style="font-size: 20px; color: #5cd08d; font-weight: bold;"></span>' +
                    '<span id="error" style="font-size: 20px; color: red; font-weight: bold;"></span>' +
                    '<br /><center><button class="btn btn-danger btn_remove" name="remove" id="'+i+'">Видалити форму</button></center>' +
                    '</div><br />');
            });

            $(document).on('click','.btn_remove', function () {
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
            })
        });
    </script>

    {{-- save/destroy data in form --}}
   <script src="/js/jquery-3.2.1.min.js"></script>
   <script src="/js/savy.min.js"></script>
    <script>
        $(function(){
            $('.w3-save').savy('load');
            $('.destroy').click(function(){
                $('.w3-save').savy('destroy');
            })
        });
    </script>

    <script>
        function answers()
        {
            var b=document.getElementById("checkbox");
            if(( b.checked == true))
            {
                document.getElementById('err').innerHTML= 'Відповідь вірна';
            }
            else
            {
                document.getElementById('error').innerHTML= 'Відповідь не вірна';
            }
        }
    </script>
 </body>
@endsection