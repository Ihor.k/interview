<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Theme Made By www.w3schools.com - No Copyright -->
    <title>Online Interview</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    <!-- Styles -->
    <link rel="stylesheet" href="https://laravelecommerceexample.ca/css/app.css">
    <link rel="stylesheet" href="https://laravelecommerceexample.ca/css/responsive.css">
    <link rel="stylesheet" href="{{asset('css/templateText.css')}}">
    <link rel="stylesheet" href="{{asset('css/recomendationResume.css')}}">
</head>

<header class="with-background">
    <div class="top-nav container">
        <div class="top-nav-left">
            <nav class="navbar navbar-expand-sm">
                <div class="logo"><a href={{asset('index')}}>Interview</a></div>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#"
                           id="navbardrop" data-toggle="dropdown">
                            Формування запитань
                        </a>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="{{asset('Question')}}"><font color="black">Запитання</font></a>
                          <a class="dropdown-item" href="{{asset('test_task')}}"><font color="black">Тестові завдання</font></a>
                        </div>
                    </li>
                    <li>
                        <a href="{{asset('aboutInterview')}}">
                            Про співбесіди
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                            Кандидату
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{asset('resume')}}"><font color="black">Рекомендації по резюме</font></a>
                            <a class="dropdown-item" href="{{asset('consultancy')}}"><font color="black">Кар'єрне консультування</font></a>
                            <a class="dropdown-item" href="http://mylifecoach.com.ua/"><font color="black">Лайф-коучинг</font></a>
                        </div>
                    </li>
                    <li>
                        <a href="{{asset('blog')}}">
                            Блоги
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="{{ route('logout') }}">Вийти</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>

<div>
    @yield('content')
</div>

<footer>
    <div class="footer-content container">
        <div class="footer__copyright">© <?php $date = date('Y'); echo $date;?> Interview</div>
        <ul>
            <li>Follow Me:</li>
            <li><a href=""><i class="fa Follow Me:"></i></a></li>
            <li>
                <a href="https://www.youtube.com/channel/UCBeZ_ZK8tnJ5C_AxBM2Fzkg?view_as=subscriber">
                    <i class="fa fa-youtube"></i>
                </a>
            </li>
            <li><a href="https://gitlab.com/"><i class="fa fa-gitlab"></i></a></li>
            <li><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://vk.com/id198459658"><i class="fa fa-vk"></i></a></li>
        </ul>
    </div>
</footer>
</html>