<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Online Interview</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    <!-- Styles -->
    <link rel="stylesheet" href="https://laravelecommerceexample.ca/css/app.css">
    <link rel="stylesheet" href="https://laravelecommerceexample.ca/css/responsive.css">


</head>
<body>
<div id="app">
    <header class="with-background">
        <div class="top-nav container">
            <div class="top-nav-left">
              <nav class="navbar navbar-expand-sm">
                <div class="logo"><a href={{asset('index')}}>Interview</a></div>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#"
                           id="navbardrop" data-toggle="dropdown">
                            Формування запитань
                        </a>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="{{asset('Question')}}"><font color="black">Запитання</font></a>
                          <a class="dropdown-item" href="{{asset('test_task')}}"><font color="black">Тестові завдання</font></a>
                        </div>
                    </li>
                    <li>
                        <a href="{{asset('aboutInterview')}}">
                            Про співбесіди
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                            Кандидату
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{asset('resume')}}"><font color="black">Рекомендації по резюме</font></a>
                            <a class="dropdown-item" href="{{asset('consultancy')}}"><font color="black">Кар'єрне консультування</font></a>
                            <a class="dropdown-item" href="http://mylifecoach.com.ua/"><font color="black">Лайф-коучинг</font></a>
                        </div>
                    </li>
                    <li>
                        <a href="{{asset('blog')}}">
                            Блоги
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="{{ route('logout') }}">Вийти</a>
                    </li>
                </ul>
              </nav>
            </div>
        </div> <!-- end top-nav -->
        <div class="hero container">
            <div class="hero-copy">
                <h1>Співбесіди онлайн</h1>
                <p>Найкращий вибір проходження співбесід - це онлайн</p>
            </div> <!-- end hero-copy -->

            <div class="hero-image">
                <style>
                    img{
                        border-radius: 50%;
                    }
                </style>
                <img src="/images/interview.jpg" alt="hero image" width="1024" height="300" class="img">
            </div> <!-- end hero-image -->
        </div> <!-- end hero -->
    </header>

    <div class="featured-section">

        <div class="container">
            <h1 class="text-center">Онлайн - співбесіда</h1>
            <p class="section-description" align="center">
                <b>
                    Це зручний інструмент пошуку роботи.
                </b>
            </p>


            <div class="circle">
                <div class="w3-container">
                    <center>
                      <a href="https://www.thebalancecareers.com/online-job-interviews-2064216">
                        <img src="/images/interview_1.jpg" class="w3-circle" alt="jobInterview" style="width:50%">
                      </a>
                    </center>
                </div>
            </div>
        </div> <!-- end container -->

    </div> <!-- end featured-section -->

    <div class="blog-section">
        <div class="container">
            <h1 class="text-center">Наші можливості</h1>
            <p class="text-center">
                Співбесіда є найважливішим етапом при прийомі на роботу.
                Саме під час співбесіди роботодавець формує своє враження про кандидата як спеціаліста і як особистість.
                Ми допомагаємо різним компаніям, організаціям та підприємствам сформувати свої запитання для того,
                щоб вибрати високо - кваліфікованих працівників онлайн.
            </p>
            <div class="blog-posts">
                <div class="blog-post">
                    <a href="https://uniskill.com.ua/main">
                        <img src="/images/testing1.jpg" class="img" alt="interview" height="230" width="400">
                    </a>
                        <h2 class="blog-title" align="center">Формування запитань</h2>
                    <div class="text-center">
                        Дана компанія визначає реальний професійний рівень кандидата та виявляє про нього
                        достовірну інформацію, яка допоможе грамотно побудувати співбесіду. Компанія формує
                        запитання і відправляє кандидату посилання для проходження співбесіди.
                    </div>
                </div>
                <div class="blog-post">
                    <a href="https://uniskill.com.ua/main">
                        <img src="/images/testing.png" alt="interview" height="230" width="400">
                    </a>
                        <h2 class="blog-title" align="center">Проходження співбесіди</h2>
                    <div class="text-center">
                        Коли компанія сформувала запитання вона відправляє кандидату посилання за яким він має
                        дати відповідь на дані запитання. Після чого він зберігає відповіді на запитання і відправляє
                        посилання з відповідями компанії.
                    </div>
                </div>
                <div class="blog-post">
                    <a href="https://uniskill.com.ua/main">
                        <img src="/images/testing2.png" height="230" width="400">
                    </a>
                        <h2 class="blog-title" align="center">Повідомлення результатів</h2>
                    <div class="text-center">
                        Коли кандидат відповів на запитання компанія проводить аналіз відповідей, перевіряє правильність
                        поставлених запитань і повідомляє кандидату результати.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="footer-content container">
            <div class="footer__copyright">© <?php $date = date('Y'); echo $date;?> Interview</div>
            <ul>
                <li>Follow Me:</li>
                <li><a href=""><i class="fa Follow Me:"></i></a></li>
                <li>
                    <a href="https://www.youtube.com/channel/UCBeZ_ZK8tnJ5C_AxBM2Fzkg?view_as=subscriber">
                        <i class="fa fa-youtube"></i>
                    </a>
                </li>
                <li><a href="https://gitlab.com/"><i class="fa fa-gitlab"></i></a></li>
                <li><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://vk.com/id198459658"><i class="fa fa-vk"></i></a></li>
            </ul>

        </div> <!-- end footer-content -->
    </footer>
</div> <!-- end #app -->
</body>
</html>
