<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="{{ asset('css/auth.css') }}">
<style>
    .img
    {
        background-repeat: no-repeat;
    }
</style>

<body background="/images/background2.jpg">
 <div class="container">
    <div class="row">
        <div class="col-md-offset-3 col-md-6">
            <br><br>
            <div class="tab" role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation"><a aria-controls="profile" role="tab" data-toggle="tab">Зареєструватися</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content tabs">
                    <div role="tabpanel">
                        <form class="form-horizontal" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Ім'я</label>
                                <input type="text" class="form-control" placeholder="Введіть ім'я" name="name" required>
                            </div>
                            <div class="form-group">
                                <label>Назва компанії</label>
                                <input type="text" class="form-control" placeholder="Введіть назву компанії" name="company" required>
                            </div>
                            <div class="form-group">
                                <label>Електронна пошта</label>
                                <input type="email" class="form-control" name="email" placeholder="Введіть пошту" required>
                            </div>
                            <div class="form-group">
                                <label>Пароль</label>
                                <input type="password" class="form-control" name="password" placeholder="Введіть пароль" required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-default"><a href="{{asset('index')}}">Sign up</a></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div><!-- /.col-md-offset-3 col-md-6 -->
    </div><!-- /.row -->
 </div><!-- /.container -->
</body>