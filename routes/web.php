<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','mainController@register');

Auth::routes();

Route::get('/logout', function(){
    Auth::logout();
    return Redirect::to('login');
});

Route::get('/home', 'HomeController@index');

Route::get('/index','mainController@index');

Route::get('/aboutInterview','mainController@aboutInterview');

Route::get('/Question/add','questionsController@add');
Route::post('/Question/add','questionsController@store');
Route::get('/Question/{id}','questionsController@show');
Route::post('/Question/{id}','questionsController@save');

Route::get('/Question','mainController@question');

Route::get('/test_task','mainController@test_tasks');

Route::get('/resume','mainController@Resume');

Route::get('/consultancy','mainController@consultancy');

Route::get('/blog','mainController@blogs');



