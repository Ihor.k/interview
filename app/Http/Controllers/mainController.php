<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class mainController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function index()
    {
        return view('index');
    }

    public function aboutInterview()
    {
        return view('aboutInterview');
    }

    public function question()
    {
        $res = \DB::select('SELECT an.id,u.name,an.position FROM ankety an
JOIN users u ON u.id = an.user_id');

        return view('Question', ['ankets' => $res]);
    }

    public function test_tasks()
    {
        return view('test_task');
    }

    public function resume()
    {
        return view('resume');
    }

    public function consultancy()
    {
        return view('consultancy');
    }

    public function blogs()
    {
        return view('blog');
    }
}
