<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class questionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    protected function _getNumbersFromRequest($request, $arr)
    {
        $items = get_object_vars($request);
        $numbers = [];
        foreach ($items['request'] as $name => $item)
        {
            if (strpos($name, $arr[0]) !== false) {
                $number = $name;
                foreach ($arr as $replaceItem) {
                    $number = str_replace($replaceItem, '', $number);
                }

                if ((int)$number) {
                    $numbers[] = (int)$number;
                }
            }
        }

        return $numbers;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $numbers = $this->_getNumbersFromRequest($request, ['answer_']);
        $isExist = true;
        foreach ($numbers as $i) {

            if (isset($request->{'answer_' . $i})) {

                $isExist = false;
                if (!isset($request->{'answer_' . $i . '_exist'})) {
                    $idAnswer = DB::table('answer')->insertGetId(
                        [
                            'question_id' => $i,
                            'answer' => $request->{'answer_' . $i},
                            'is_right' => null,
                            'created_at' => now(),
                            'updated_at' => now()
                        ]
                    );
                }
            }
        }

        $numbers = $this->_getNumbersFromRequest($request, ['answer_', '_exist_id']);

        if ($isExist) {
            foreach ($numbers as $i) {
                if (isset($request->{'answer_' . $i . '_exist_id'})) {
                    $isRight = isset($request->{'checkbox_' . $i}) ? 1 : 0;
                    DB::table('answer')
                        ->where('id', $request->{'answer_' . $i . '_exist_id'})
                        ->update(['is_right' => $isRight]);
                }
            }
        }

        return redirect('/Question')->with('message', 'Anketa saved!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $idAnkety = DB::table('ankety')->insertGetId([
            'user_id' => $request->{'user_id'},
            'position' => $request->{'position'},
            'created_at' => now(),
            'updated_at' => now()
        ]);

        if  ($idAnkety) {

            $numbers = $this->_getNumbersFromRequest($request, ['question_']);

            foreach ($numbers as $i) {
                if (isset($request->{'question_' . $i})) {
                    $idQuestion = DB::table('question')->insertGetId(
                        [
                            'ankety_id' => $idAnkety,
                            'question' => $request->{'question_' . $i},
                            'created_at' => now(),
                            'updated_at' => now()
                        ]
                    );
                }
            }
        }

        return redirect('/Question')->with('message', 'Anketa created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $res = \DB::select('SELECT q.id as question_id,q.*,an.*,a.id as answer_id, a.answer, a.is_right FROM ankety an
JOIN question q ON an.id = q.ankety_id
LEFT JOIN answer a ON a.question_id = q.id
WHERE an.id = ' . $id);

        return view('QuestionsEdit', ['questions' => $res]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function add()
    {
        $res = \DB::select('SELECT * FROM users u');

        return view('Questions', ['users' => $res]);
    }
}
